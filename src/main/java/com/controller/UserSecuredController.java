package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.service.AppUserService;

@RequestMapping("/secured/")
@RestController
public class UserSecuredController extends WebMvcConfigurerAdapter {

	@Autowired
	private AppUserService service;

	@RequestMapping("/remove/{id}")
	public void removeUser(@PathVariable("id") long id) {
		service.removeUser(id);
	}
}
