package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.model.AppUser;
import com.service.AppUserService;
import com.service.SecurityService;
import com.validator.UserValidator;

@RequestMapping("/")
@RestController
public class UserController extends WebMvcConfigurerAdapter {

	@Autowired
	private AppUserService service;
	
	@Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

	
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody Iterable<AppUser> findAllUsers() {
		return service.getAllUsers();
	}

	@GetMapping("/add")
	public @ResponseBody String addNewUser(@RequestParam String name) {
		service.addUser(name);
		return "Saved";
	}

	@RequestMapping(value = "/update/{id}/{name}", method = RequestMethod.GET)
	public AppUser updateUser(@PathVariable("id") long id, @PathVariable String name) {
		return service.updateUser(id, name);
	}

	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public ResponseEntity<AppUser> findUser(@PathVariable("id") long id) {
		AppUser u = service.getUserById(id);
		if (u == null)
			return ResponseEntity.badRequest().build();
		return ResponseEntity.ok(u);
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new AppUser());

        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") AppUser userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        service.saveUser(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "welcome";
    }
}

