package com.service;

import com.model.AppUser;

public interface AppUserService {

	AppUser addUser(String name);
	
	AppUser getUserById(long id);
	
	AppUser updateUser(long id, String name);

	Iterable<AppUser> getAllUsers();

	void removeUser(long id);
	
	void saveUser(AppUser user);
	
	AppUser findByUsername(String username);
}
