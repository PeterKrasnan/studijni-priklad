package com.service;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.model.AppUser;
import com.model.AppUserRepository;
import com.model.RoleRepository;

@Service
@Transactional
public class AppUserServiceImpl implements AppUserService {

	private final AppUserRepository repository;
	private final RoleRepository roleRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public AppUserServiceImpl(AppUserRepository repository, RoleRepository roleRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.repository = repository;
		this.roleRepository = roleRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public AppUser addUser(String name) {
		AppUser newUser = new AppUser(name);
		repository.save(newUser);
		return newUser;
	}

	@Override
	public AppUser getUserById(long id) {
		return repository.findOne(id);
	}

	@Override
	public AppUser updateUser(long id, String name) {
		AppUser user = repository.findOne(id);
		user.setName(name);
		repository.save(user);
		return user;
	}

	@Override
	public Iterable<AppUser> getAllUsers() {
		return repository.findAll();
	}

	@Override
	public void removeUser(long id) {
		repository.delete(id);
	}

	@Override
	public void saveUser(AppUser user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRoles(new HashSet<>(roleRepository.findAll()));
		repository.save(user);
	}

	@Override
	public AppUser findByUsername(String username) {
		return repository.findByUsername(username);
	}
}
